<!DOCTYPE html>
<html>
    <head>

      <meta charset="utf-8">
      <title>API Engenharia de Software</title>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body>

        <div class="row">
            <div class="col s12 m6 push-m3">
                <h5 class="center"> Soma / Multiplicação </h5>
                <form action="api.php" method="POST">
                    <div class="input-field col s12">
                        <input type="text" name="valor1" id="valor1">
                        <label for="valor1">1º operando</label>
                    </div>

                    <div class="input-field col s12">
                        <input type="text" name="valor2" id="valor2">
                        <label for="valor2">2º operando</label>
                    </div>

                    <button type="submit" name="btn-calcular" class="btn"> Calcular </button>

                </form>

            </div>   
        </div>
    </body>
</html>


<?php

if(isset($_POST['btn-calcular'])) {
    $valor1 = $_POST['valor1'];
    $valor2 = $_POST['valor2'];

    if(!(is_numeric($valor1) and is_numeric($valor2))) {
        echo "Informe apenas valores numéricos!";
    }
    else {
        soma($valor1, $valor2);
        produto($valor1, $valor2);
    }
}

function soma($valor1, $valor2) {
    if(verSoma($valor1, $valor2) == true) {
        $soma = $valor1 + $valor2;
        echo "Soma = ".$soma;
    }
    else {
        echo "Não satisfaz os axiomas da soma!";
    }
}

function produto($valor1, $valor2) {
    if(verProduto($valor1, $valor2) == true and (($valor1 or $valor2) != 0)) {
        echo "<br>";
        $produto = $valor1 * $valor2;
        echo "Produto = ".$produto;
    }
    else {
        echo "<br>";
        echo "Não satisfaz os axiomas da multiplicação!";
    }
}

function verSoma($valor1, $valor2) {
    $z = 2;

    if(($valor1 + $valor2) + $z == $valor1 + ($valor2 + $z) and
        $valor1 + $valor2 == $valor2 + $valor1 and
        $valor1 + 0 == $valor1 and $valor2 + 0 == $valor2 and
        $valor1 + (-$valor1) == 0 and $valor2 + (-$valor2) == 0)
        
        return true;
    else {
        return false;
    } 

}

function verProduto($valor1, $valor2) {
    $z = 2;

    if(($valor1 * $valor2) * $z == $valor1 * ($valor2 * $z) and
        $valor1 * $valor2 == $valor2 * $valor1 and
        $valor1 * 1 == $valor1 and $valor2 * 1 == $valor2 and
        $valor1 * pow($valor1, -1) == 1 and $valor2 * pow($valor2, -1) == 1) {
            
        return true;
    }
    else {
        return false;
    }
}